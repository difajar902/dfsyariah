import React, { useState } from "react";
import { Text, StyleSheet } from "react-native";

const TextInANest = () => {
    const [titleText, setTitleText] = useState("DFsyariah");
    const bodyText = useState("DFsyariah is a simple project");

    const onPressTitle = () => {
        setTitleText("DFsyariah [pressed]");
    };

    return ( <
        Text style = { styles.baseText } >
        <
        Text style = { styles.titleText }
        onPress = { onPressTitle } > { titleText } { "\n" } { "\n" } <
        /Text> <
        Text numberOfLines = { 5 } > { bodyText } < /Text> < /
        Text >
    );
};

const styles = StyleSheet.create({
    baseText: {
        fontFamily: "Cochin"
    },
    titleText: {
        fontSize: 15
        fontWeight: "bold"
    }
});

export default TextInANest;